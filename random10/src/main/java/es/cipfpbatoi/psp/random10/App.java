package es.cipfpbatoi.psp.random10;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       //Instanciamos los objetos que vamos a utilizar
       int aleatorio;
       Scanner sc = new Scanner(System.in);
       File prueba = new File("randoms.txt");
       
       try{
       //Instaciamos el Buffered para escribir en el fichero   
       BufferedWriter bw = new BufferedWriter(new FileWriter(prueba));
       System.out.println("Hola, si me escribes stop no te dare ningun número");
       
       String palabra;
       
       //Comparamos la cadena de texto que mete el usuario con la palabra stop
       while(!(palabra= sc.nextLine()).equals("stop")) {
    	   
    	   //Instaciamos el random entre 1 y 10
    	   aleatorio = (int) (Math.random()*10+1);
    	   System.out.println("Numero aleatorio -> " +aleatorio);
    	   
    	   //Escribimos el numero aleatorio en el fichero
    	   bw.write("Este es el numero -> "+aleatorio);
    	   bw.newLine(); 
    	   bw.flush();
       }
       
       	   System.out.println("Has escrito stop adios");
       
       	   bw.close();
    	   
  	 
       	
	   
    	}catch(IOException e) {
    		e.printStackTrace();
    	}
   	}
}
