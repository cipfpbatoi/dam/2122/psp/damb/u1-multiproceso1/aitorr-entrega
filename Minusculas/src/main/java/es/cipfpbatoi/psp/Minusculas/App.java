package es.cipfpbatoi.psp.Minusculas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	Scanner sc = new Scanner(System.in);
        File prueba = new File("Minusculas.txt");
        System.out.println("La palabra que me escribas te la paso a minuscula");
    	
        try{
        	
        BufferedWriter bw = new BufferedWriter(new FileWriter(prueba));
        String palabra;
        while(!(palabra=sc.nextLine()).equals("FINALIZAR")) {
        System.out.println(palabra.toLowerCase());
        bw.write(palabra);
        bw.newLine();
        bw.flush();
    	}
        
        System.out.println("Has finalizado el programa");
        bw.close();
        
        }catch(IOException e) {
    		e.printStackTrace();
    	
    }
}
}
